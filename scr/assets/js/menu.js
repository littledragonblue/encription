const menuApp = '<a onclick="javascript:setPageHash()" id="menuHash" style="color:yellow">Hash</a> | ' +
    '<a onclick="javascript:setPageEncode()" id="menuEncode">Encode</a> | ' +
    '<a onclick="javascript:setPageDecode()" id="menuDecode">Decode</a> | ' +
    '<a onclick="javascript:setPageEncrypt()" id="menuEncrypt">Encrypt</a> | ' +
    '<a onclick="javascript:setPageDecrypt()" id="menuDecrypt">Decrypt</a> | ' +
    '<a onclick="javascript:setPageRSA()" id="menuRSA">RSA</a>';

document.getElementById("menu").innerHTML = menuApp;

function setPageHash() {
    page = "hash";

    document.getElementById("error").innerHTML = null;
    document.querySelector('select').innerHTML = null;
    document.getElementById("spanMethodTitle").innerHTML = lang['Method'];
    arrayHash = crypto.getHashes();
    createHashList();

    $('#hashstring').attr('placeholder', lang['WriteHere'] + lang['or'] + lang['DropFileHere']);
    $('#secretKeyTitle').attr('style', "display:none;");
    $('#noteSecretKey').attr('style', "display:none;");
    $('#secretKey').attr('style', "display:none;");
    $('#menuHash').attr('style', "color:yellow");
    $('#menuEncode').attr('style', "");
    $('#menuDecode').attr('style', "");
    $('#menuEncrypt').attr('style', "");
    $('#menuDecrypt').attr('style', "");
    $('#menuRSA').attr('style', "");
    $('#btngenerator').attr('style', "");
}

function setPageEncode() {
    page = "encode";

    document.getElementById("error").innerHTML = null;
    document.querySelector('select').innerHTML = null;
    document.getElementById("spanMethodTitle").innerHTML = lang['Method'];
    arrayHash = ['base64'];
    createHashList();

    $('#hashstring').attr('placeholder', lang['WriteHere'] + lang['or'] + lang['DropFileHere']);
    $('#secretKeyTitle').attr('style', "display:none;");
    $('#noteSecretKey').attr('style', "display:none;");
    $('#secretKey').attr('style', "display:none;");
    $('#menuHash').attr('style', "");
    $('#menuEncode').attr('style', "color:yellow");
    $('#menuDecode').attr('style', "");
    $('#menuEncrypt').attr('style', "");
    $('#menuDecrypt').attr('style', "");
    $('#menuRSA').attr('style', "");
    $('#btngenerator').attr('style', "");
}

function setPageDecode() {
    page = "decode";

    document.getElementById("error").innerHTML = null;
    document.querySelector('select').innerHTML = null;
    document.getElementById("spanMethodTitle").innerHTML = lang['Method'];
    arrayHash = ['base64'];
    createHashList();

    $('#hashstring').attr('placeholder', lang['WriteHere']);
    $('#secretKeyTitle').attr('style', "display:none;");
    $('#noteSecretKey').attr('style', "display:none;");
    $('#secretKey').attr('style', "display:none;");
    $('#menuHash').attr('style', "");
    $('#menuEncode').attr('style', "");
    $('#menuDecode').attr('style', "color:yellow");
    $('#menuEncrypt').attr('style', "");
    $('#menuDecrypt').attr('style', "");
    $('#menuRSA').attr('style', "");
    $('#btngenerator').attr('style', "");
}

function setPageEncrypt() {
    page = "encrypt";

    document.getElementById("error").innerHTML = null;
    document.querySelector('select').innerHTML = null;
    document.getElementById("spanMethodTitle").innerHTML = lang['Method'];
    arrayHash = crypto.getCiphers();
    createHashList();

    $('#hashstring').attr('placeholder', lang['WriteHere'] + lang['or'] + lang['DropFileHere']);
    $('#secretKeyTitle').attr('style', "");
    $('#secretKey').attr('style', "width:100%;");
    $('#noteSecretKey').attr('style', "");
    $('#menuHash').attr('style', "");
    $('#menuEncode').attr('style', "");
    $('#menuDecode').attr('style', "");
    $('#menuEncrypt').attr('style', "color:yellow");
    $('#menuDecrypt').attr('style', "");
    $('#menuRSA').attr('style', "");
    $('#btngenerator').attr('style', "");
}

function setPageDecrypt() {
    page = "decrypt";

    document.getElementById("error").innerHTML = null;
    document.querySelector('select').innerHTML = null;
    document.getElementById("spanMethodTitle").innerHTML = lang['Method'];
    arrayHash = crypto.getCiphers();
    createHashList();

    $('#hashstring').attr('placeholder', lang['WriteHere'] + lang['or'] + lang['DropFileHere']);
    $('#secretKeyTitle').attr('style', "");
    $('#secretKey').attr('style', "width:100%;");
    $('#noteSecretKey').attr('style', "display:none;");
    $('#menuHash').attr('style', "");
    $('#menuEncode').attr('style', "");
    $('#menuDecode').attr('style', "");
    $('#menuEncrypt').attr('style', "");
    $('#menuDecrypt').attr('style', "color:yellow");
    $('#menuRSA').attr('style', "");
    $('#btngenerator').attr('style', "");
}

function setPageRSA() {
    page = "rsa";

    document.getElementById("error").innerHTML = null;
    document.querySelector('select').innerHTML = null;
    document.getElementById("spanMethodTitle").innerHTML = lang['File'];
    arrayHash = ['privateKey', 'publicKey'];
    createHashList();

    $('#hashstring').attr('placeholder', lang['WriteHere'] + ' & ' + lang['DropFileHere']);
    $('#secretKeyTitle').attr('style', "display:none");
    $('#secretKey').attr('style', "display:none");
    $('#noteSecretKey').attr('style', "display:block");
    document.getElementById('noteSecretKey').innerHTML = lang['RSAdialog'];
    $('#menuHash').attr('style', "");
    $('#menuEncode').attr('style', "");
    $('#menuDecode').attr('style', "");
    $('#menuEncrypt').attr('style', "");
    $('#menuDecrypt').attr('style', "");
    $('#menuRSA').attr('style', "color:yellow");
    $('#btngenerator').attr('style', "display:none");
}