/**
 * Hash String
 */
const crypto = require('crypto');
const NodeRSA = require('node-rsa');
const { LOADIPHLPAPI } = require('dns');
const { app } = require('electron');
let arrayHash = crypto.getHashes();

function createHashList() {
    const select = document.querySelector('select');

    arrayHash.forEach(item => {
        const option = document.createElement('option');
        const itemText = document.createTextNode(item);
        option.appendChild(itemText);
        select.appendChild(option);
    });
}

function getShaMethod() {
    hashMethod = document.getElementById("posthash").value;
}

function getHash(encoding = 'utf-8') {
    const timeStart = performance.now();
    let resulthash = document.getElementById("resulthash");
    resulthash.innerHTML = null;
    $('#btnCopyClipboard').attr('style', "display:none;");
    let string = document.getElementById("hashstring").value;
    const filePath = document.getElementById("info").innerHTML;

    if (filePath != "") {
        getShaMethod();

        if (page == 'decode') {
            document.getElementById("error").innerHTML = lang['ActionNotAvailable'];
            return;
        } else if (page == 'encode') {
            return encodeFile(filePath, timeStart);
        } else if (page == 'encrypt') {
            return execEncryptFile(string, filePath, timeStart);
        } else if (page == 'decrypt') {
            return execDecryptFile(string, filePath, timeStart);
        } else if (page == 'rsa') {
            return execRSA(string, filePath, timeStart);
        } else {
            getChecksum(filePath)
                .then(checksum => resulthash.innerHTML = checksum)
                .catch(err => app.quit);
            const timeEnd = performance.now();
            const timeExec = ((timeEnd - timeStart) + ' ' + lang['milliseconds']);
            document.getElementById("info").innerHTML = filePath;
            document.getElementById("timeExec").innerHTML = timeExec;
            $('#btnCopyClipboard').attr('style', "");
            return;
        }
    }

    if (!string) {
        document.getElementById("error").innerHTML = lang['PleaseInsertString'];
        return;
    }

    if (page == 'encode') {
        getShaMethod();
        let newHash = getEncoded(string)
        resulthash.innerHTML = newHash;

        const timeEnd = performance.now();
        const timeExec = ((timeEnd - timeStart) + ' ' + lang['milliseconds']);
        document.getElementById("timeExec").innerHTML = timeExec;
        $('#btnCopyClipboard').attr('style', "");
        return newHash;
    } else if (page == 'decode') {
        getShaMethod();
        let newHash = getDecoded(string)
        if (!newHash) {
            document.getElementById("error").innerHTML = lang['Invalidkey'];
            return;
        }
        resulthash.innerHTML = newHash;

        const timeEnd = performance.now();
        const timeExec = ((timeEnd - timeStart) + ' ' + lang['milliseconds']);
        document.getElementById("timeExec").innerHTML = timeExec;
        $('#btnCopyClipboard').attr('style', "");
        return newHash;
    } else if (page == 'encrypt') {
        getShaMethod();
        let newHash = encryptString(string);
        resulthash.innerHTML = newHash;

        const timeEnd = performance.now();
        const timeExec = ((timeEnd - timeStart) + ' ' + lang['milliseconds']);
        document.getElementById("timeExec").innerHTML = timeExec;
        $('#btnCopyClipboard').attr('style', "");
        return newHash;
    } else if (page == 'decrypt') {
        getShaMethod();
        let secretKey = document.getElementById("secretKey").value;

        if (secretKey == "") {
            document.getElementById("error").innerHTML = lang['Invalidkey'];
            return;
        }

        let newHash = decryptString(string, secretKey);

        if (newHash === undefined) {
            return;
        }

        resulthash.innerHTML = newHash;
        const timeEnd = performance.now();
        const timeExec = ((timeEnd - timeStart) + ' ' + lang['milliseconds']);
        document.getElementById("timeExec").innerHTML = timeExec;
        $('#btnCopyClipboard').attr('style', "");
        return newHash;
    }

    document.getElementById("error").innerHTML = null;
    let hash = crypto.createHash(hashMethod);
    let data = hash.update(string, encoding);
    let newHash = data.digest('hex');

    resulthash.innerHTML = newHash;

    const timeEnd = performance.now();
    const timeExec = ((timeEnd - timeStart) + ' ' + lang['milliseconds']);
    document.getElementById("timeExec").innerHTML = timeExec;
    $('#btnCopyClipboard').attr('style', "");
    return newHash;
}

/**
 * Hash File
 */
let importFile;

document.addEventListener('drop', (event) => {
    event.preventDefault();
    event.stopPropagation();

    for (const f of event.dataTransfer.files) {
        importFile = f.path;
    }

    if (importFile) {
        let timeStart = performance.now();

        if (page == 'encode') {
            return encodeFile(importFile, timeStart);
        }

        if (page == 'encrypt') {
            let string = document.getElementById("hashstring").value;
            if (!string) {
                document.getElementById("error").innerHTML = lang['PleaseInsertString'];
                return;
            }
            return execEncryptFile(string, importFile, timeStart);
        }

        if (page == 'decrypt') {
            let string = document.getElementById("hashstring").value;
            if (!string) {
                document.getElementById("error").innerHTML = lang['PleaseInsertString'];
                return;
            }
            return execDecryptFile(string, importFile, timeStart);
        }

        if (page == 'rsa') {
            let string = document.getElementById("hashstring").value;
            if (!string) {
                document.getElementById("error").innerHTML = lang['PleaseInsertString'];
                return;
            }
            return execRSA(string, importFile, timeStart);
        }


        let resulthash = document.getElementById("resulthash");
        resulthash.innerHTML = null;

        getChecksum(importFile)
            .then(checksum => resulthash.innerHTML = checksum);

        const timeEnd = performance.now();
        const timeExec = ((timeEnd - timeStart) + ' ' + lang['milliseconds']);
        document.getElementById("info").innerHTML = importFile;
        document.getElementById("timeExec").innerHTML = timeExec;
    }
});

document.addEventListener('dragover', (e) => {
    e.preventDefault();
    e.stopPropagation();

    $('#hashstring').attr('class', "p5 br5 form placeholderDrag");
});

document.addEventListener('dragenter', (event) => {
    // TODO
    //$('#hashstring').attr('class', "p5 br5 form placeholderDrag");
});

document.addEventListener('dragleave', (event) => {
    $('#hashstring').attr('class', "p5 br5 form");
});

function getChecksum(path) {
    return new Promise((resolve, reject) => {
        const hash = crypto.createHash(hashMethod);
        const input = fs.createReadStream(path);
        input.on('error', reject);
        input.on('data', (chunk) => {
            hash.update(chunk);
        });
        input.on('close', () => {
            resolve(hash.digest('hex'));
        });
    });
}


/**
 * Base Encoding / Decoding
 */
function getEncoded(string) {
    try {
        return Buffer.from(string).toString(hashMethod);
    } catch (error) {
        return false;
    }
}

function getDecoded(string) {
    try {
        return Buffer.from(string, hashMethod).toString('utf-8');
    } catch (error) {
        return false;
    }
}

function encodeFile(filePath, timeStart) {
    getShaMethod();

    let newHash = fs.readFileSync(filePath, { encoding: hashMethod });
    resulthash.innerHTML = newHash;

    const timeEnd = performance.now();
    const timeExec = ((timeEnd - timeStart) + ' ' + lang['milliseconds']);
    document.getElementById("timeExec").innerHTML = timeExec;
    $('#btnCopyClipboard').attr('style', "");
}

/**
 * Encryption with key
 */
function encryptString(string) {
    let secretKey = document.getElementById("secretKey").value;

    if (secretKey == "") {
        secretKey = crypto.createHash('sha256').update(String(crypto.randomBytes(32))).digest('base64').substr(0, 32);
        document.getElementById("secretKey").value = secretKey;
    }

    const cipher = crypto.createCipher(hashMethod, secretKey);
    let encryptedData = cipher.update(string, "utf-8", "hex");
    encryptedData += cipher.final("hex");

    return encryptedData;
};

function decryptString(hash, secretKey) {
    const decipher = crypto.createDecipher(hashMethod, secretKey);

    try {
        let decryptedData = decipher.update(hash, "hex", "utf-8");
        decryptedData += decipher.final("utf8");
        return decryptedData;
    } catch (error) {
        document.getElementById("error").innerHTML = lang['Invalidkey'];
        return;
    }
};


function encryptWithFile(string, filePublicKey) {
    try {
        const publicKey = fs.readFileSync(filePublicKey, "utf8");
        const key = new NodeRSA(publicKey);

        const encrypted = key.encrypt(string, 'base64');
        return encrypted
    } catch (error) {
        return false;
    }
}

function decryptWithFile(string, filePublicKey) {
    try {
        var publicKey = fs.readFileSync(filePublicKey, "utf8");
        const key = new NodeRSA(publicKey);

        const encrypted = key.decrypt(string, 'utf8');
        return encrypted
    } catch (error) {
        return false;
    }
}

function execEncryptFile(string, filePath, timeStart) {
    getShaMethod();
    let newHash = encryptWithFile(string, filePath);

    if (!newHash) {
        document.getElementById("error").innerHTML = lang['InvalidFile'];
        return;
    }
    resulthash.innerHTML = newHash;

    const timeEnd = performance.now();
    const timeExec = ((timeEnd - timeStart) + ' ' + lang['milliseconds']);
    document.getElementById("timeExec").innerHTML = timeExec;
    $('#btnCopyClipboard').attr('style', "");
    return newHash;
}

function execDecryptFile(string, filePath, timeStart) {
    getShaMethod();
    let newHash = decryptWithFile(string, filePath);

    if (!newHash) {
        document.getElementById("error").innerHTML = lang['InvalidFile'];
        return;
    }
    resulthash.innerHTML = newHash;

    const timeEnd = performance.now();
    const timeExec = ((timeEnd - timeStart) + ' ' + lang['milliseconds']);
    document.getElementById("timeExec").innerHTML = timeExec;
    $('#btnCopyClipboard').attr('style', "");
    return newHash;
}


/**
 * Encrypting data with a public key in Node.js
 */
function encryptWithRsaPubliKey(string, filePublicKey) {
    try {
        var publicKey = fs.readFileSync(filePublicKey, "utf8");
        const key = new NodeRSA(publicKey);

        const encrypted = key.encryptPrivate(string, 'base64');
        return encrypted
    } catch (error) {
        return false;
    }
}

function decryptWithRsaPubliKey(string, filePublicKey) {
    try {
        var publicKey = fs.readFileSync(filePublicKey, "utf8");
        const key = new NodeRSA(publicKey);

        const encrypted = key.decryptPublic(string, 'utf8');
        return encrypted
    } catch (error) {
        return false;
    }
}

function execRSA(string, filePath, timeStart) {
    getShaMethod();
    let newHash;

    if (hashMethod == 'publicKey') {
        newHash = decryptWithRsaPubliKey(string, filePath);
    } else if (hashMethod == 'privateKey') {
        newHash = encryptWithRsaPubliKey(string, filePath);
    }

    if (!newHash) {
        document.getElementById("error").innerHTML = lang['InvalidFile'];
        return;
    }
    resulthash.innerHTML = newHash;

    const timeEnd = performance.now();
    const timeExec = ((timeEnd - timeStart) + ' ' + lang['milliseconds']);
    document.getElementById("timeExec").innerHTML = timeExec;
    $('#btnCopyClipboard').attr('style', "");
    return newHash;
}