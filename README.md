# [<img src="https://samoreira.eu/assets/icon/passwordGenerator.png" align="left" width="32px" style="padding-right:10"/> Encryption ](https://samoreira.eu/app.php?encryption)

[<img src="https://upload.wikimedia.org/wikipedia/commons/9/91/Electron_Software_Framework_Logo.svg" align="right" width="128px">](https://electronjs.org)


> Electron is an open-source framework for creating desktop apps using web technologies. It combines the Chromium rendering engine and the Node.js runtime.

> This app was build to start learn about electron technologie.

<br>

# Documentation
[Electron API](https://www.electronjs.org/docs/v14-x-y/api/app)

# Start App
```
npm start
```

# Install Dependencies
```
npm install
```

# Error on Debian
<code>Building electron linux distro : The SUID sandbox helper binary was found, but is not configured correctly:</code>
```
sudo sysctl kernel.unprivileged_userns_clone=1
```

# Author

* [Sérgio Moreira](https://samoreira.eu)

# Main Categories
## Linux
By including one of the Main Categories in an application's desktop entry file, the application will be ensured that it will show up in a section of the application menu dedicated to this category. If multiple Main Categories are included in a single desktop entry file, the entry may appear more than once in the menu.

Category-based menus based on the Main Categories listed in this specification do not provide a complete ontology for all available applications. Category-based menu implementations SHOULD therefore provide a "catch-all" submenu for applications that cannot be appropriately placed elsewhere.

<code>AudioVideo, Audio, Video, Development, Education, Game, Graphics, Network, Office, Science, Settings, System, Utility</code>

## Apple
*LSApplicationCategoryType*

LSApplicationCategoryType (String - macOS) is a string that contains the UTI corresponding to the app’s type. The App Store uses this string to determine the appropriate categorization for the app. Table 2 lists the supported UTIs for apps.

### UTIs for app categories

* **Business** -> public.app-category.business
* **Developer Tools** -> public.app-category.developer-tools
* **Education** -> public.app-category.education
* **Entertainment** -> public.app-category.entertainment
* **Finance** -> public.app-category.finance
* **Games** -> public.app-category.games
* **Graphics & Design** -> public.app-category.graphics-design
* **Healthcare & Fitness** -> public.app-category.healthcare-fitness
* **Lifestyle** -> public.app-category.lifestyle
* **Medical** -> public.app-category.medical
* **Music** -> public.app-category.music
* **News** -> public.app-category.news
* **Photography** -> public.app-category.photography
* **Productivity** -> public.app-category.productivity
* **Reference** -> public.app-category.reference
* **Social Networking** -> public.app-category.social-networking
* **Sports** -> public.app-category.sports
* **Travel** -> public.app-category.travel
* **Utilities** -> public.app-category.utilities
* **Video** -> public.app-category.video
* **Weather** -> public.app-category.weather

### UTIs for game-specific categories

* **Action Games** -> public.app-category.action-games
* **Adventure Games** -> public.app-category.adventure-games
* **Arcade Games** -> public.app-category.arcade-games
* **Board Games** -> public.app-category.board-games
* **Card Games** -> public.app-category.card-games
* **Casino Games** -> public.app-category.casino-games
* **Dice Games** -> public.app-category.dice-games
* **Educational Games** -> public.app-category.educational-games
* **Family Games** -> public.app-category.family-games
* **Kids Games** -> public.app-category.kids-games
* **Music Games** -> public.app-category.music-games
* **Puzzle Games** -> public.app-category.puzzle-games
* **Racing Games** -> public.app-category.racing-games
* **Role Playing Games** -> public.app-category.role-playing-games
* **Simulation Games** -> public.app-category.simulation-games
* **Sports Games** -> public.app-category.sports-games
* **Strategy Games** -> public.app-category.strategy-games
* **Trivia Games** -> public.app-category.trivia-games
* **Word Games** -> public.app-category.word-games